#!/usr/bin/env python
import rospy
from temp1.msg import Num

def func_callback(data):
    print data.id,"!!!\n"
    rospy.loginfo(rospy.get_caller_id() + 'I heard %s', data.st)

def listener():

    rospy.init_node('listener', anonymous=True)
    rospy.Subscriber('chatter', Num, func_callback)

    rospy.spin()

if __name__ == '__main__':
    listener()
