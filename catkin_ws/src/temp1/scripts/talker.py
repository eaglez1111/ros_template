#!/usr/bin/env python
import rospy
from temp1.msg import Num

def talker():
    pub = rospy.Publisher('chatter', Num, queue_size=10)
    rospy.init_node('talker', anonymous=True)
    rate = rospy.Rate(10) # 10hz

    msg = Num()
    msg.id=0
    msg.st="hello world ***"+str(msg.id)

    while not rospy.is_shutdown():
        msg.id = msg.id+1
        msg.st="hello world ***"+str(msg.id)
        rospy.loginfo(msg)
        pub.publish(msg)
        rate.sleep()

if __name__ == '__main__':
    try:
        talker()
    except rospy.ROSInterruptException:
        pass
