#include "ros/ros.h"
#include "std_msgs/String.h"
#include "temp0/Num.h"

#include <sstream>

int main(int argc, char **argv)
{

  ros::init(argc, argv, "talker");

  ros::NodeHandle n;

  ros::Publisher chatter_pub = n.advertise<temp0::Num>("chatter", 1000);

  ros::Rate loop_rate(10);

  int count = 0;
  while (ros::ok())
  {

    temp0::Num msg;

    std::stringstream ss;
    ss << "hello world " << count << "***";
    msg.st = ss.str();
    msg.id=count;

    msg.a.push_back(3);
    msg.a.push_back(1);
    msg.a.push_back(4);

    chatter_pub.publish(msg);

    ROS_INFO("%s", msg.st.c_str());


    ros::spinOnce();

    loop_rate.sleep();
    ++count;
  }


  return 0;
}
